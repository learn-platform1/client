import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/scss/main.scss'
import VueMultiSelect from "vue-simple-multi-select"

Vue.component('vue-multi-select', VueMultiSelect);
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/group',
    name: 'group',
    component: () => import('../views/Group.vue')
  },
  {
    path: '/grouplist',
    name: 'grouplist',
    component: () => import('../views/GroupList.vue')
  },
  {
    path: '/groupedit/:id',
    name: 'groupedit',
    component: () => import('../views/GroupEdit.vue')
  },
  {
    path: '/exception',
    name: 'exception',
    component: () => import('../views/Exception.vue')
  },
  {
    path: '/node',
    name: 'node',
    component: () => import('../views/Node.vue')
  },
  {
    path: '/nodeedit/:id',
    name: 'nodeedit',
    component: () => import('../views/NodeEdit.vue')
  },
  {
    path: '/nodelist',
    name: 'nodelist',
    component: () => import('../views/NodeList.vue')
  },
  {
    path: '/permission',
    name: 'permission',
    component: () => import( '../views/Permission.vue')
  },
  {
    path: '/exercise',
    name: 'exercise',
    component: () => import('../views/Exercise.vue')
  },
  {
    path: '/exerciselist',
    name: 'exerciselist',
    component: () => import('../views/ExerciseList.vue')
  },
  {
    path: '/exerciseedit/:id',
    name: 'exerciseedit',
    component: () => import('../views/ExerciseEdit.vue')
  },
  {
    path: '/exercisesolve/:id',
    name: 'exercisesolve',
    component: () => import('../views/ExerciseSolve.vue')
  },
  {
    path: '/exam/:id',
    name: 'exam',
    component: () => import('../views/Exam.vue')
  },
  {
    path: '/logout',
    name: 'logout',
    component: () => import('../views/Logout.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
